#include <iostream>
#include <string>
#include <vector>

#include "dkeep/logic/Game.h"

int AskNumberOfDragons();
void PrintMaze(std::vector<std::vector<char>> &maze);

int main(int argc, char** argv) {

  char uc;
  bool is_game_over = false;

  int ndragons = AskNumberOfDragons();

  dkeep::logic::Game *game = new dkeep::logic::Game(ndragons);

  do {

    // print the maze
    //PrintMaze(game->GetMaze());
    game->maze->PrintMaze();

    // read user command
    std::cout << "cmd> ";
    std::cin >> uc;

    // ignore the other characters
    std::cin.clear();
    std::cin.ignore(INT_MAX, '\n');

    // update the game
    is_game_over = game->UpdateGame(uc); //(*game).UpdateGame(uc);
    if (!is_game_over)
      std::cout << game->GetOutputMessage() << std::endl;

  } while ((!is_game_over) && (uc != 'q'));

  // final state of the game
  //PrintMaze(game->GetMaze());
  game->maze->PrintMaze();
  std::cout << game->GetOutputMessage() << std::endl;

  std::cout << "Exiting!" << std::endl;

  // delete dynamically allocated variables
  delete game;
}


/// Returns valid integer number of dragons from console
/// Loops console until a valid integer is entered
int AskNumberOfDragons() 
{
        #if EXCEPTIONS 
                // Demo code to show exceptions handling
                char n;
                do {
                    std::cout << "How many dragons in the maze? (1-4) ";

                    std::cin >> n;
                    std::string nstr(1,n);

                    std::cin.clear();
                    std::cin.ignore(INT_MAX, '\n');

                    // std::atoi does not throw an exception when the conversion was not possible
                    // for exemplification purposes, we are using std::stoi to use exceptions

                    try {
                        int nd = std::stoi(nstr);
                        if ((nd <= 0) || (nd > 4))
                            throw std::invalid_argument("Number must be between 1 and 4");
                        
                        return nd;
                    } catch (std::invalid_argument& e) {
                        std::cout << "Please enter a valid number" << std::endl;
                    }
                } while (true);
    #else 
                // Demo code without exceptions
                int ndragons;
                do {
                    std::cout << "How many dragons in the maze? (1-4) ";
                    std::cin >> ndragons;
                    if (std::cin.fail()||(ndragons<=0)||(ndragons>4)) {
                        std::cout << "Please enter a valid number" << std::endl;
                        std::cin.clear();               // Clear error flag
                        std::cin.ignore(INT_MAX, '\n'); // Trash all input
                } else {
                        std::cout << "Using " << ndragons << " dragons" << std::endl;
                        break;
                    }
                } while (true);
                return ndragons;
    #endif

}


