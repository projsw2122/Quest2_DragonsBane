# DragonsBane (C++ version)

Written for the "Software Design" course of M.EEC, in FEUP, Portugal

Main contributers: 
- Armando Sousa    [email](mailto:asousa@fe.up.pt)
- Ricardo B. Sousa [email](mailto:rbs@fe.up.pt)

Text, plot of Quest and videos (below): 
- Nuno Flores [email](nflores@fe.up.pt)

## License

[GPL v3](https://www.gnu.org/licenses/gpl-3.0.html)

In short, free software: Useful, transparent, no warranty


## Description


The mini-quest "Dragon's Bane" is supposed to reuse the results from mini-quest
I "Here Be Dragons". Concepts such as classes and objects should be used for
implementing the mini-quest. This mini-quest is implemented in C++ and was
compiled in VS Code. See the [`guide`](doc/MiniQuest2DragonsBane.md)
for more information about the mini-quest.

Only the common part of the mini-quest is implemented in C++, just as in the
Java version. The videos available for the Java version are the following ones:

- Package separation ([video](https://www.youtube.com/watch?v=ob-8gabEB_M))
- New Game Logic ([video](https://www.youtube.com/watch?v=GcybhYNrOo8))
- Dragon movement and multiple dragons
  ([video](https://www.youtube.com/watch?v=TfEQHLTe3_E))

The YouTube video for the Java version is also valid for C++. You should have
the following considerations in C++:

- Namespaces:
  - Consider equivalent to packages in Java
  - Separate different namespaces in different folders insider `src/` and
    `include/`
- Classes: usually, separate different classes in different `cpp/h` files




## Usage

### Makefile

```sh
# Clean
$ mingw32-make clean
# Build
$ mingw32-make
# Run
$ mingw32-make run
```

### Tasks on VS Code

1. Open `src/main.cpp`
2. Build `DragonsBane.exe`
   - VS Code > Terminal > Run Build Task (`Ctrl+Shift+B`)
3. Run `DragonsBane.exe`
   - VS Code > Run > Start debugging (`F5`)
   - OR VS Code > Run > Run without debugging (`Ctrl+F5`)

### Make (in PowerShell, under MS Windows)
```
PS> make clean
PS> make
PS> make run
```


## Documentation of objects

game <- dkeep::logic::Game(ndragons);

dkeep::logic::Game
    | maze      <- Maze();
    | hero      <- Hero(maze, 1, 1);
    | sword     <- Element(maze, 8, 1);
    | dragons[] <- std::vector<Dragon*>(ndragons); 

    (to be done...)
