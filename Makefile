# Usage:
# - Windows (always use a bash terminal)
# mingw32-make					# build DragonsBane.exe
# mingw32-make run			# run DragonsBane.exe
# mingw32-make clean		# remove ALL binaries and objects
# - Linux:
# make					# build DragonsBane.exe
# make run			# run DragonsBane.exe
# make clean		# remove ALL binaries and objects

#Windows detector - find rm in PATH
ifeq ($(wildcard $(addsuffix /rm,$(subst :, ,$(PATH)))),)
	WINMODE=1
else
	WINMODE=0
endif

.PHONY = all run clean

default: all

# Compiler to use
CXX = g++

# Flags
SRCS_FLAGS = -g -std=c++11 -Iinclude -Wall 


OBJS := Maze.o Element.o MoveableElement.o Hero.o Dragon.o Game.o

###########################################################
ifeq ($(WINMODE),1)  # native windows:
###########################################################

all: DragonsBane.exe

run: DragonsBane.exe
	.\$<

DragonsBane.exe: src\main.cpp $(OBJS)
	${CXX} ${SRCS_FLAGS} $< $(OBJS) -o $@

%.o: src\dkeep\logic\%.cpp
	@echo "Creating object $@.."
	${CXX} ${SRCS_FLAGS} -c $^

clean:
	@echo "Cleaning up..."
	rm -rf *.o *.exe

###########################################################
else #Unix like
###########################################################

all:  DragonsBane

run: DragonsBane
	./$<

DragonsBane: src/main.cpp $(OBJS)
	${CXX} ${SRCS_FLAGS} $< $(OBJS) -o $@

%.o: src/dkeep/logic/%.cpp
	@echo "Creating object $@.."
	${CXX} ${SRCS_FLAGS} -c $^

clean:
	@echo "Cleaning up..."
	rm -f *.o DragonsBane

###########################################################
endif
###########################################################
